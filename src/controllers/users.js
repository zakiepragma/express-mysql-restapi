const getAllUsers = (req, res) => {
  res.json({
    message: "Get all user",
  });
};

const createNewUser = (req, res) => {
  res.json({
    message: "Create new user",
  });
};

module.exports = {
  getAllUsers,
  createNewUser,
};

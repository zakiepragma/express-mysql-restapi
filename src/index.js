const express = require("express");

const app = express();

const port = 4000;

const userRoutes = require("./routes/users");
const logRequest = require("./middleware/logs");

app.use(logRequest);

app.use(express.json());

app.use("/users", userRoutes);

app.listen(port, () => {
  console.log(`Server berjalan di port ${port}`);
});
